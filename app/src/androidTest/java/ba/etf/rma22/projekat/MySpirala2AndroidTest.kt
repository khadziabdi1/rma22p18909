//package ba.etf.rma22.projekat
//
//import androidx.recyclerview.widget.RecyclerView
//import androidx.test.espresso.Espresso.onData
//import androidx.test.espresso.Espresso.onView
//import androidx.test.espresso.action.ViewActions.click
//import androidx.test.espresso.assertion.ViewAssertions.matches
//import androidx.test.espresso.contrib.RecyclerViewActions
//import androidx.test.espresso.matcher.ViewMatchers.*
//import androidx.test.ext.junit.rules.ActivityScenarioRule
//import androidx.test.ext.junit.runners.AndroidJUnit4
//import ba.etf.rma22.projekat.data.repositories.AnketaRepository
//import org.hamcrest.CoreMatchers
//import org.hamcrest.Matchers.`is`
//import org.hamcrest.Matchers.instanceOf
//import org.hamcrest.core.AllOf.allOf
//import org.junit.Rule
//import org.junit.Test
//import org.junit.runner.RunWith
//
//@RunWith(AndroidJUnit4::class)
//class MySpirala2AndroidTest {
//    @get:Rule
//    val intentsTestRule = ActivityScenarioRule(MainActivity::class.java)
//
//    @Test
//    fun testZadatak1() {
//        onView(withId(R.id.pager)).perform(ViewPager2Actions.scrollToFirst())
//        onView(withId(R.id.pager)).perform(ViewPager2Actions.scrollToPosition(1))
//        onView(withId(R.id.odabirGodina)).perform(click())
//        onData(CoreMatchers.allOf(CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)), CoreMatchers.`is`("2"))).perform(click())
//        onView(withId(R.id.dodajIstrazivanjeDugme)).perform(click())
//        onView(withId(R.id.pager)).perform(ViewPager2Actions.scrollToFirst())
//        onView(withId(R.id.pager)).perform(ViewPager2Actions.scrollToPosition(1))
//        onView(withId(R.id.odabirGodina)).perform(click())
//        onData(CoreMatchers.allOf(CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)), CoreMatchers.`is`("3"))).perform(click())
//        onView(withId(R.id.dodajIstrazivanjeDugme)).perform(click())
//        onView(withSubstring("Uspješno ste upisani u grupu")).check(matches(isDisplayed()))
//    }
//
//    @Test
//    fun testZadatak2() {
//        onView(withId(R.id.pager)).perform(ViewPager2Actions.scrollToFirst())
//        onView(withId(R.id.pager)).perform(ViewPager2Actions.scrollToPosition(1))
//        onView(withId(R.id.odabirGodina)).perform(click())
//        onData(CoreMatchers.allOf(CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)), CoreMatchers.`is`("2"))).perform(click())
//        onView(withId(R.id.dodajIstrazivanjeDugme)).perform(click())
//        onView(withId(R.id.pager)).perform(ViewPager2Actions.scrollToFirst())
//        val ankete = AnketaRepository.getMyAnkete()
//        onView(withId(R.id.listaAnketa)).perform(RecyclerViewActions.actionOnItem<RecyclerView.ViewHolder>(CoreMatchers.allOf(hasDescendant(withText(ankete[1].naziv)),
//            hasDescendant(withText(ankete[1].nazivIstrazivanja))), click()))
//        onView(withId(R.id.dugmeZaustavi)).perform(click())
//        onView(withId(R.id.listaAnketa)).perform(RecyclerViewActions.actionOnItem<RecyclerView.ViewHolder>(CoreMatchers.allOf(hasDescendant(withText(ankete[1].naziv)),
//            hasDescendant(withText(ankete[1].nazivIstrazivanja))), click()))
//        onData(allOf(`is`(instanceOf(String::class.java)), `is`("Prvi3"))).perform(click())
//        onView(withId(R.id.pager)).perform(ViewPager2Actions.scrollToLast())
//        onView(withId(R.id.dugmePredaj)).perform(click())
//        onView(withId(R.id.pager)).perform(ViewPager2Actions.scrollToFirst())
//        onView(withId(R.id.dugmeZaustavi)).perform(click())
//    }
//
//}