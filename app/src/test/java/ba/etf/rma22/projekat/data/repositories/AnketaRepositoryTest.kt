//package ba.etf.rma22.projekat.data.repositories
//
//import org.junit.Test
//import org.junit.Assert.*
//import org.junit.FixMethodOrder
//import org.junit.runners.MethodSorters
//
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
//class AnketaRepositoryTest {
//    @Test
//    fun testGetMyAnkete(){
//        assertEquals(3,AnketaRepository.getMyAnkete().size)
//    }
//    @Test
//    fun testGetMyAnkete2(){
//        KorisnikRepository.dodajIstrazivanje("Beta","Cetvrta")
//        assertEquals(4,AnketaRepository.getMyAnkete().size)
//    }
//    @Test
//    fun testGetAll(){
//        assertEquals(10,AnketaRepository.getAll().size)
//    }
//    @Test
//    fun testGetDone(){
//        assertEquals(0,AnketaRepository.getDone().size)
//    }
//    @Test
//    fun testGetDone2(){
//        KorisnikRepository.dodajIstrazivanje("Gama","Sesta")
//        assertEquals(1,AnketaRepository.getDone().size)
//    }
//    @Test
//    fun testGetFuture(){
//        assertEquals(0,AnketaRepository.getFuture().size)
//    }
//    @Test
//    fun testGetFuture2(){
//        KorisnikRepository.dodajIstrazivanje("Epsilon","Deseta")
//        assertEquals(1,AnketaRepository.getFuture().size)
//    }
//    @Test
//    fun testGetNotTaken(){
//        assertEquals(0,AnketaRepository.getNotTaken().size)
//    }
//    @Test
//    fun testGetNotTaken2(){
//        KorisnikRepository.dodajIstrazivanje("Delta","Sedma")
//        assertEquals(1,AnketaRepository.getNotTaken().size)
//    }
//
//}