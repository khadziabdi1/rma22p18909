//package ba.etf.rma22.projekat.data.repositories
//
//import org.junit.Test
//import org.junit.Assert.*
//import org.junit.Before
//import org.junit.FixMethodOrder
//import org.junit.runners.MethodSorters
//
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
//class KorisnikRepositoryTest {
//    @Before
//    fun vratiNaPocetak(){
//        KorisnikRepository.user = korisnik()
//    }
//    @Test
//    fun dodajIstrazivanjeTest(){
//        val p = KorisnikRepository.user.grupeNaIstrazivanjima.size
//        KorisnikRepository.dodajIstrazivanje("Beta","Treca")
//        assertEquals(p+1,KorisnikRepository.user.grupeNaIstrazivanjima.size)
//    }
//    @Test
//    fun dodajIstrazivanjeTest2(){
//        KorisnikRepository.dodajIstrazivanje("Gama","Peta")
//        assertEquals("PrvaPeta",KorisnikRepository.user.grupeNaIstrazivanjima["Alfa"]+KorisnikRepository.user.grupeNaIstrazivanjima["Gama"])
//    }
//}