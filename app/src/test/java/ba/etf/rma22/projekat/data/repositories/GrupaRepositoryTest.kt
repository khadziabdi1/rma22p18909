//package ba.etf.rma22.projekat.data.repositories
//
//import ba.etf.rma22.projekat.data.models.Grupa
//import org.junit.Test
//import org.junit.Assert.*
//import org.junit.Before
//import org.junit.FixMethodOrder
//import org.junit.runners.MethodSorters
//
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
//class GrupaRepositoryTest {
//    @Before
//    fun vratiNaPocetak(){
//        KorisnikRepository.user = korisnik()
//    }
//    @Test
//    fun getGroupsByIstrazivanjeTest(){
//        assertEquals(listOf(Grupa("Prva","Alfa"),Grupa("Druga","Alfa")),GrupaRepository.getGroupsByIstrazivanje("Alfa"))
//    }
//    @Test
//    fun getGroupsByIstrazivanjeTest2(){
//        assertEquals(listOf(Grupa("Peta","Gama"),Grupa("Sesta","Gama")),GrupaRepository.getGroupsByIstrazivanje("Gama"))
//    }
//}