//package ba.etf.rma22.projekat.data.repositories
//
//import org.junit.Test
//import org.junit.Assert.*
//import org.junit.Before
//import org.junit.FixMethodOrder
//import org.junit.runners.MethodSorters
//
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
//class IstrazivanjeRepositoryTest {
//    @Before
//    fun vratiNaPocetak(){
//        KorisnikRepository.user = korisnik()
//    }
//    @Test
//    fun getIstrazivanjeByGodinaTest(){
//        assertEquals(3,IstrazivanjeRepository.getIstrazivanjeByGodina(2).size)
//    }
//    @Test
//    fun getIstrazivanjeByGodinaTest2(){
//        assertEquals (1,IstrazivanjeRepository.getIstrazivanjeByGodina(1).size)
//    }
//    @Test
//    fun getAllTest(){
//        assertEquals(5,IstrazivanjeRepository.getAll().size)
//    }
//    @Test
//    fun getUpisani(){
//        assertEquals(1,IstrazivanjeRepository.getUpisani().size)
//    }
//    @Test
//    fun getUpisani2(){
//        assertEquals("Alfa",IstrazivanjeRepository.getUpisani()[0].naziv)
//    }
//    @Test
//    fun getUpisani3(){
//        KorisnikRepository.dodajIstrazivanje("Beta","Treca")
//        assertEquals("Beta",IstrazivanjeRepository.getUpisani()[1].naziv)
//    }
//}