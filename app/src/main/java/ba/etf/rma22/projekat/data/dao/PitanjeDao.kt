package ba.etf.rma22.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import ba.etf.rma22.projekat.data.models.Pitanje

@Dao
interface PitanjeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPitanje(pitanje: Pitanje)
}