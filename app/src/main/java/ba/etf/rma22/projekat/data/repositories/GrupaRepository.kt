package ba.etf.rma22.projekat.data.repositories

import ba.etf.rma22.projekat.AppDatabase
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.models.Grupa
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object GrupaRepository {
    private val db = AppDatabase.getInstance(MainActivity.context_!!)
    suspend fun getGrupeOdStudenta(acHash:String=AccountRepository.getHash()):List<Grupa>{
        return withContext(Dispatchers.IO){
            val response = ApiConfig.retrofit.getGrupeOdStudenta(acHash)
            val responseBody = response.body()
            if(responseBody == null) return@withContext listOf()
            else {
                for(i in responseBody){
                    db.grupaDao().insertGrupa(i)
                }
                return@withContext responseBody
            }
        }
    }
}