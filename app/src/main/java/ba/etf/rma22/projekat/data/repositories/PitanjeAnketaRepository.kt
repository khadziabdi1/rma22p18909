package ba.etf.rma22.projekat.data.repositories

import ba.etf.rma22.projekat.AppDatabase
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.models.Pitanje
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object PitanjeAnketaRepository {
    private val db = AppDatabase.getInstance(MainActivity.context_!!)
    suspend fun getPitanja(idAnkete: Int):List<Pitanje>?{
        return withContext(Dispatchers.IO){
            val response = ApiConfig.retrofit.getPitanja(idAnkete)
            val responseBody = response.body()
            if(responseBody == null || responseBody.isEmpty()) return@withContext null
            else {
                for(i in responseBody)
                    db.pitanjeDao().insertPitanje(i)
                return@withContext responseBody
            }
        }
    }
}