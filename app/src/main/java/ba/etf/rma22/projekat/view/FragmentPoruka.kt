package ba.etf.rma22.projekat.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import ba.etf.rma22.projekat.R

class FragmentPoruka private constructor() : Fragment() {
    private lateinit var tvPoruka: TextView
    private var poruka: String? = ""
    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view:View = inflater.inflate(R.layout.poruka_fragment,container,false)
        tvPoruka = view.findViewById(R.id.tvPoruka)
        poruka = arguments?.getString("Poruka")
        tvPoruka.text = poruka
        return view
    }
    companion object {
        fun newInstance(): FragmentPoruka = FragmentPoruka()
    }
}