package ba.etf.rma22.projekat.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.viewmodel.AccountViewModel

class OdgovorListAdapter(context: Context,private val odgovori: List<String>, private val idPitanja:Long):ArrayAdapter<String>(context,R.layout.odgovor_layout,odgovori) {
    @SuppressLint("ViewHolder")
    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val pogled = LayoutInflater.from(context).inflate(R.layout.odgovor_layout,parent,false)
        val textView = pogled.findViewById<TextView>(R.id.text_view)
        val element = odgovori[position]
        textView.text = element
        if(!AccountViewModel.odgovori.none{ it.idPitanje == idPitanja }){

            textView.setTextColor(Color.parseColor("#0000FF"))
        }
        return pogled
    }
}