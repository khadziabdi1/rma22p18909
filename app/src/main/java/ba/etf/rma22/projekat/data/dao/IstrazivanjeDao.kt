package ba.etf.rma22.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma22.projekat.data.models.Istrazivanje

@Dao
interface IstrazivanjeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertIstrazivanje(istrazivanje: Istrazivanje)
    @Query("select * from istrazivanje")
    suspend fun getAll(): List<Istrazivanje>
}