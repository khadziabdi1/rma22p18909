package ba.etf.rma22.projekat.data.repositories

import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.AppDatabase
import ba.etf.rma22.projekat.data.models.Anketa
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

object AnketaRepository {
    private val db = AppDatabase.getInstance(MainActivity.context_!!)
    suspend fun getAll(offset:Int = 0):List<Anketa>{
        if(!MainActivity.getConnection())
            return db.anketaDao().getAll()
        return withContext(Dispatchers.IO){
            if(offset == 0){
                var abortFlag = false
                var count = 1
                val returnBody = arrayListOf<Anketa>()

                while(!abortFlag){
                    val response = ApiConfig.retrofit.getAll(count)
                    val responseBody = response.body() ?: return@withContext returnBody
                    if(responseBody.size < 5)abortFlag = true
                    returnBody.addAll(responseBody)
                    count ++
                }
                for(it in returnBody){
                    db.anketaDao().insertAnketa(it)
                }
                return@withContext returnBody
            }
            val response = ApiConfig.retrofit.getAll(offset)
            val responseBody = response.body()
            if(responseBody == null) return@withContext listOf()
            else {
                for(it in responseBody){
                    db.anketaDao().insertAnketa(it)
                }
                return@withContext responseBody
            }
        }
    }

    suspend fun getById(id:Int):Anketa?{
        if(!MainActivity.getConnection())
            return db.anketaDao().getById(id)
        return withContext(Dispatchers.IO) {
            val response = ApiConfig.retrofit.getById(id)
            db.anketaDao().insertAnketa(response.body()!!)
            return@withContext response.body()
        }
    }

    suspend fun getUpisane():List<Anketa>{
        if(!MainActivity.getConnection())
            return db.anketaDao().getAll()
        val grupe = IstrazivanjeIGrupaRepository.getUpisaneGrupe()
        val rez = arrayListOf<Anketa>()
        for (i in grupe) {
            rez.addAll(getAnketeGrupe(i.id))
        }
        return rez
    }

    private suspend fun getAnketeGrupe(idGrupe:Int):List<Anketa>{
        if(!MainActivity.getConnection())
            return db.anketaDao().getAll()
        return withContext(Dispatchers.IO){
            val response = ApiConfig.retrofit.getAnketeGrupe(idGrupe)
            val responseBody = response.body()
            if(responseBody == null) return@withContext arrayListOf<Anketa>()
            else {
                for (i in responseBody){
                    db.anketaDao().insertAnketa(i)
                }
                return@withContext responseBody
            }
        }
    }
}

