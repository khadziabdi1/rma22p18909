package ba.etf.rma22.projekat.data.models

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity
data class Anketa(
    @PrimaryKey@SerializedName("id")var id:Int,
    @SerializedName("naziv")var naziv:String,
    @SerializedName("datumPocetak")var datumPocetak:Date,
    @SerializedName("datumKraj")var datumKraj:Date?,
    @SerializedName("trajanje")var trajanje:Int,
    var progres: Float = 0f,
    var nazivIstrazivanja: String? = "",
    var zavrsena: Boolean =false,
    var datumRada: Date? = null,
    var prosla: Boolean = false
)
