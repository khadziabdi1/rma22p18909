package ba.etf.rma22.projekat.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity
data class AnketaTaken(
    @PrimaryKey@SerializedName("id")var id:Int,
    @SerializedName("student")var student: String?,
    @SerializedName("progres")var progres: Float,
    @SerializedName("datumRada")var datumRada: Date?,
    @SerializedName("AnketumId")var AnketumId: Int
)
