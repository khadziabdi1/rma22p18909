package ba.etf.rma22.projekat.data.repositories

import ba.etf.rma22.projekat.AppDatabase
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.models.Account
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

object AccountRepository {
    val db= AppDatabase.getInstance(MainActivity.getContext())
    var acHash:String = "9bdbcb7c-c108-41f6-a87e-1401d09833d3"
    fun postaviHash(acHash: String):Boolean{
        GlobalScope.launch(Dispatchers.IO) {
            db.clearAllTables()
            db.accountDao().addAccount(Account(acHash))
        }
        this.acHash = acHash
        return true
    }
    fun getHash():String{
        return acHash
    }
}