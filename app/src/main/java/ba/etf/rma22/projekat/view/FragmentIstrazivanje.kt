package ba.etf.rma22.projekat.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.Fragment
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.data.models.Grupa
import ba.etf.rma22.projekat.viewmodel.AccountViewModel
import ba.etf.rma22.projekat.viewmodel.IstrazivanjeIgrupaViewModel

class FragmentIstrazivanje: Fragment() {
    private lateinit var odabirGrupa: Spinner
    private lateinit var dugme: Button
    private lateinit var komunikator: Komunikator
    override fun onCreateView(inflater: LayoutInflater,container: ViewGroup?,savedInstanceState: Bundle?): View {
        val view: View = inflater.inflate(R.layout.istrazivanje_fragment,container,false)
        odabirGrupa = view.findViewById(R.id.odabirGrupa)
        dugme = view.findViewById(R.id.dodajIstrazivanjeDugme)
        IstrazivanjeIgrupaViewModel.getGrupe(onSuccess = ::onSuccess2)
        komunikator = activity as Komunikator
        dugme.setOnClickListener {
            AccountViewModel .dodajGrupu(odabirGrupa.selectedItem as Grupa, onSuccess = ::onSuccess, onError = ::onError)
        }
        if(!MainActivity.getConnection()){
            dugme.isActivated = false
            dugme.isClickable = false
        }
        return view
    }
    companion object{
        fun newInstance(): FragmentIstrazivanje = FragmentIstrazivanje()
    }
    private fun onSuccess() {
        komunikator.proslijediPodatke(odabirGrupa.selectedItem as Grupa)
    }
    private fun onError(){
        val toast = Toast.makeText(context, "Group error", Toast.LENGTH_SHORT)
        toast.show()
    }
    private fun onSuccess2(grupe:List<Grupa>){
        IstrazivanjeIgrupaViewModel.getUpisaneGrupe{gp -> grupe.filter { !gp.contains(it)}}
        odabirGrupa.adapter = ArrayAdapter(requireActivity(),android.R.layout.simple_spinner_item,grupe)
    }

    override fun onResume() {
        if(!MainActivity.getConnection()){
            dugme.isActivated = false
            dugme.isClickable = false
        }else{
            dugme.isActivated = true
            dugme.isClickable = true
        }
        super.onResume()
    }
}