package ba.etf.rma22.projekat.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class PitanjeAnketa(
    @PrimaryKey@SerializedName("AnketumId")val idAnkete:Int,
    @SerializedName("PitanjeId")val idPitanja:Int
)
