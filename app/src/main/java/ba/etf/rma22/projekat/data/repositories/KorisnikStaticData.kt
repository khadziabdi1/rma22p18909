package ba.etf.rma22.projekat.data.repositories

import ba.etf.rma22.projekat.data.models.Korisnik

fun korisnik(): Korisnik{
    return Korisnik(mapOf(Pair("Alfa","Prva")).toSortedMap(),
        mapOf<String,String>().toSortedMap()
    )
}