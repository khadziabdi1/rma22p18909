package ba.etf.rma22.projekat.view

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.view.forEach
import androidx.fragment.app.Fragment
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.viewmodel.OdgovorViewModel

//class FragmentPitanje: Fragment() {
//    private lateinit var sifraPitanja: String
//    private lateinit var tekstPitanja: TextView
//    private lateinit var dugmeZaustavi: Button
//    private lateinit var odgovoriLista: ListView
//    private lateinit var komunikator: Komunikator
//    override fun onCreateView(
//        inflater: LayoutInflater,
//        container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View? {
//        val view = inflater.inflate(R.layout.pitanje_fragment,container,false)
//        tekstPitanja = view.findViewById(R.id.tekstPitanja)
//        komunikator = activity as Komunikator
//        dugmeZaustavi = view.findViewById(R.id.dugmeZaustavi)
//        odgovoriLista = view.findViewById(R.id.odgovoriLista)
//        tekstPitanja.text = arguments?.getString("TekstPitanja")
//        sifraPitanja = arguments?.getString("SifraPitanja").toString()
//        val listaOdgovora:ArrayList<String> = arguments?.getStringArrayList("Odgovori")!!
//        odgovoriLista.adapter = OdgovorListAdapter(requireActivity(),listaOdgovora, requireArguments().getLong("PitanjeId"))
//        odgovoriLista.setOnItemClickListener { _, pogled, _, _ ->
//            OdgovorViewModel.postaviOdgovorAnketa(requireArguments().getInt("AnketaId"),requireArguments().getLong("PitanjeId"),listaOdgovora.indexOf(pogled.findViewById<TextView>(R.id.text_view).text))
//            odgovoriLista.forEach {
//                val i = it.findViewById<TextView>(R.id.text_view)
//                i.setTextColor(Color.parseColor("#000000"))
//            }
//            pogled.findViewById<TextView>(R.id.text_view).setTextColor(Color.parseColor("#0000FF"))
//        }
//        if(KorisnikRepository.user.odgovori.containsKey(sifraPitanja)){
//            odgovoriLista.forEach {
//                val i = it.findViewById<TextView>(R.id.text_view)
//                if(i.text.toString() == KorisnikRepository.user.odgovori[sifraPitanja])
//                    i.setTextColor(Color.parseColor("#0000FF"))
//            }
//        }
//        dugmeZaustavi.setOnClickListener{
//            komunikator.zaustaviAnketu(arguments?.getString("Anketa")!!,arguments?.getString("Istrazivanje")!!)
//        }
//        if(arguments?.getBoolean("Gotova")!! || arguments?.getBoolean("Prosla")!!) {
//            odgovoriLista.isClickable = false
//            odgovoriLista.isEnabled = false
//        }
//        return view
//    }
//
//    companion object{
//        fun newInstance():FragmentPitanje = FragmentPitanje()
//    }
//
//    override fun onResume() {
//        if(KorisnikRepository.user.odgovori.containsKey(sifraPitanja)){
//            odgovoriLista.forEach {
//                val i = it.findViewById<TextView>(R.id.text_view)
//                if(i.text.toString() == KorisnikRepository.user.odgovori[sifraPitanja])
//                    i.setTextColor(Color.parseColor("#0000FF"))
//            }
//        }
//        super.onResume()
//    }
//
//}