package ba.etf.rma22.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import ba.etf.rma22.projekat.data.models.Odgovor

@Dao
interface OdgovorDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOdgovor(odgovor: Odgovor)
}