package ba.etf.rma22.projekat.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import ba.etf.rma22.projekat.R

//class FragmentPredaj: Fragment() {
//    private lateinit var progresTekst: TextView
//    private lateinit var dugmePredaj: Button
//    private lateinit var listaPitanja: ArrayList<String>
//    private lateinit var komunikator: Komunikator
//    private var progres: Double = 0.0
//    @SuppressLint("SetTextI18n")
//    override fun onCreateView(
//        inflater: LayoutInflater,
//        container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View {
//        val view: View = inflater.inflate(R.layout.predaj_fragment,container,false)
//        komunikator = activity as Komunikator
//        progresTekst = view.findViewById(R.id.progresTekst)
//        dugmePredaj = view.findViewById(R.id.dugmePredaj)
//        listaPitanja = arguments?.getStringArrayList("Pitanja")!!
//        var j = 0
//        for(i in listaPitanja){
//            if(KorisnikRepository.user.odgovori.containsKey(i))
//                j++
//        }
//        progres = j.toDouble()/listaPitanja.size.toDouble()
//        when {
//            progres<0.1 -> {
//                progresTekst.text = "0%"
//            }
//            progres<0.30 -> {
//                progresTekst.text = "20%"
//            }
//            progres<0.50 -> {
//                progresTekst.text = "40%"
//            }
//            progres<0.70 -> {
//                progresTekst.text = "60%"
//            }
//            progres<0.90 -> {
//                progresTekst.text = "80%"
//            }
//            else -> {
//                progresTekst.text = "100%"
//            }
//        }
//        dugmePredaj.setOnClickListener{
//            komunikator.predajAnketu(progres,arguments?.getString("Anketa")!!,arguments?.getString("Istrazivanje")!!)
//        }
//        if(arguments?.getBoolean("Gotova")!! || arguments?.getBoolean("Prosla")!!) {
//            dugmePredaj.isEnabled = false
//            dugmePredaj.isClickable =false
//        }
//        return view
//    }
//    companion object{
//        fun newInstance(): FragmentPredaj = FragmentPredaj()
//    }
//
//    override fun onResume() {
//        super.onResume()
//        var j = 0
//        for(i in listaPitanja){
//            if(KorisnikRepository.user.odgovori.containsKey(i))
//                j++
//        }
//        progres = j.toDouble()/listaPitanja.size.toDouble()
//        when {
//            progres<0.1 -> {
//                progresTekst.text = "0%"
//            }
//            progres<0.30 -> {
//                progresTekst.text = "20%"
//            }
//            progres<0.50 -> {
//                progresTekst.text = "40%"
//            }
//            progres<0.70 -> {
//                progresTekst.text = "60%"
//            }
//            progres<1 -> {
//                progresTekst.text = "80%"
//            }
//            else -> {
//                progresTekst.text = "100%"
//            }
//        }
//    }
//}