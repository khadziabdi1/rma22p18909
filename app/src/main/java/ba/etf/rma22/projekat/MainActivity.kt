package ba.etf.rma22.projekat

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper

import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.Grupa
import ba.etf.rma22.projekat.data.models.Pitanje
import ba.etf.rma22.projekat.data.repositories.AccountRepository
import ba.etf.rma22.projekat.view.*

class MainActivity : AppCompatActivity(), Komunikator {
    private lateinit var viewPager: ViewPager2
    private lateinit var cld: ConnectionLiveData
    private lateinit var viewPagerAdapter: ViewPagerAdapter
    private var fragmentAnkete = FragmentAnkete.newInstance()
    private var fragmentIstrazivanje = FragmentIstrazivanje.newInstance()
    companion object{
        @SuppressLint("StaticFieldLeak")
        var context_: Context? = null
        private var connection_: Boolean = false
        fun getContext():Context{
            return context_!!
        }
        fun setContext(context: Context){
            context_ = context
        }
        fun getConnection(): Boolean{
            return connection_
        }
        fun setConnection(boolean: Boolean){
            println(boolean)
            connection_ = boolean
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        setContext(this)
        checkNetworkConnection()
        setHashIntent()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewPager=findViewById(R.id.pager)
        viewPagerAdapter = ViewPagerAdapter(supportFragmentManager,mutableListOf(), lifecycle)
        viewPagerAdapter.add(0,fragmentAnkete)
        viewPagerAdapter.add(1,fragmentIstrazivanje)
        viewPager.adapter = viewPagerAdapter
    }

    private fun checkNetworkConnection() {
        cld = ConnectionLiveData(application)
        cld.observe(this) { setConnection(it) }
    }

    fun setHashIntent(){
        if (intent != null){
            var acHash = intent.getStringExtra("payload")
            if(acHash == null){
                var uri: Uri? = intent.data
                if(uri != null){
                    var params: List<String> = uri.pathSegments.toList()
                    acHash = params[params.size -1]
                }
                else acHash = AccountRepository.acHash
            }
            AccountRepository.postaviHash(acHash)
        }
    }

    override fun proslijediPodatke(g:Grupa) {
        val bundle = Bundle()
        bundle.putString("Poruka","Uspješno ste upisani u grupu ${g.naziv}!")
        val fragmentPoruka = FragmentPoruka.newInstance()
        fragmentPoruka.arguments = bundle
        Handler(Looper.getMainLooper()).post{
            viewPagerAdapter.remove(1)
            viewPagerAdapter.add(1,fragmentPoruka)
        }
    }

    override fun osvjeziPocetniEkran() {
            Handler(Looper.getMainLooper()).post{
                if(viewPagerAdapter.itemCount >= 2){
                    viewPagerAdapter.remove(1)
                    viewPagerAdapter.add(1, fragmentIstrazivanje)
                }
            }
    }
    private var listaPitanja: List<Pitanje> = listOf()
   override fun klikNaAnketu(anketa: Anketa) {
//        Handler(Looper.getMainLooper()).post{
//            viewPagerAdapter.remove(1)
//            viewPagerAdapter.remove(0)
//            PitanjeAnketaViewModel.getPitanja(anketa.id, onSuccess = ::onSuccess)
//            var j = 0
//            for (i in listaPitanja){
//                val bundle = Bundle()
//                bundle.putString("TekstPitanja",i.tekstPitanja)
//                bundle.putString("SifraPitanja",i.naziv)
//                bundle.putStringArrayList("Odgovori",i.opcije)
//                bundle.putLong("PitanjeId",i.id)
//                bundle.putString("Anketa",anketa.naziv)
//                bundle.putInt("AnketId",anketa.id)
//                bundle.putString("Istrazivanje",anketa.nazivIstrazivanja)
//                bundle.putBoolean("Gotova",anketa.zavrsena)
//                bundle.putBoolean("Prosla",anketa.prosla)
//                val fragmentPitanje= FragmentPitanje()
//                fragmentPitanje.arguments = bundle
//                viewPagerAdapter.add(j,fragmentPitanje)
//                j++
//            }
//            val fragmentPredaj: FragmentPredaj = FragmentPredaj.newInstance()
//            val bundle2 = Bundle()
//            bundle2.putStringArrayList("Pitanja", listaPitanja.map { it.naziv } as ArrayList<String>)
//            bundle2.putString("Anketa",anketa.naziv)
//            bundle2.putString("Istrazivanje",anketa.nazivIstrazivanja)
//            bundle2.putBoolean("Gotova",anketa.zavrsena)
//            bundle2.putBoolean("Prosla",anketa.prosla)
//            fragmentPredaj.arguments = bundle2
//            viewPagerAdapter.add(j,fragmentPredaj)
        }

//    fun onSuccess(pitanja: List<Pitanje>){
//        listaPitanja = pitanja
//    }

    override fun predajAnketu(progres: Double, anketa: String, istrazivanje: String) {
//        Handler(Looper.getMainLooper()).post{
//            viewPagerAdapter.removeAll()
//            var bundle = Bundle()
//            bundle.putDouble("Progres",progres)
//            bundle.putString("Anketa",anketa)
//            bundle.putString("Istrazivanje",istrazivanje)
//            fragmentAnkete.arguments = bundle
//            viewPagerAdapter.add(0,fragmentAnkete)
//            bundle = Bundle()
//            bundle.putString("Poruka","Završili ste anketu $anketa u okviru istraživanja $istrazivanje")
//            val poruka = FragmentPoruka.newInstance()
//            poruka.arguments = bundle
//            viewPagerAdapter.add(1,poruka)
//            viewPager.currentItem = 1
//            AnketaRepository.update(anketa,istrazivanje,true)
//        }
    }

        override fun zaustaviAnketu(anketa: String, istrazivanje: String) {
//        viewPagerAdapter.removeAll()
//        val bundle = Bundle()
//        val progres: Double
//        val listaPitanja: List<String> = PitanjeAnketaRepository.getPitanja(anketa,istrazivanje).map { it.naziv }.toList()
//        var j = 0
//        for(i in listaPitanja){
//            if(KorisnikRepository.user.odgovori.containsKey(i))
//                j++
//        }
//        progres = j.toDouble()/listaPitanja.size.toDouble()
//        bundle.putDouble("Progres",progres)
//        bundle.putString("Anketa",anketa)
//        bundle.putString("Istrazivanje", istrazivanje)
//        fragmentAnkete.arguments = bundle
//        viewPagerAdapter.add(0,fragmentAnkete)
//        viewPagerAdapter.add(1,fragmentIstrazivanje)
//        viewPager.currentItem = 0
    }

}