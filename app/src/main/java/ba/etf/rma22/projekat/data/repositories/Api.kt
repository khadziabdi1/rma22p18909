package ba.etf.rma22.projekat.data.repositories

import ba.etf.rma22.projekat.data.models.*
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface Api {
    @GET("/anketa/{id}/pitanja")
    suspend fun getPitanja(@Path("id") id:Int):Response<List<Pitanje>>
    @GET("/istrazivanje/{id}")
    suspend fun getIstrazivanje(@Path("id")id:Int):Response<Istrazivanje>
    @GET("/anketa")
    suspend fun getAll(@Query("offset")offset:Int):Response<List<Anketa>>
    @GET("/anketa/{id}")
    suspend fun getById(@Path("id") id:Int):Response<Anketa>
    @GET("/student/{id}/grupa")
    suspend fun getGrupeOdStudenta(@Path("id")id:String):Response<List<Grupa>>
    @GET("/istrazivanje")
    suspend fun getIstrazivanja(@Query("offset")offset:Int):Response<List<Istrazivanje>>
    @GET("/grupa")
    suspend fun getGrupe():Response<List<Grupa>>
    @GET("/grupa/{id}/ankete")
    suspend fun getAnketeGrupe(@Path("id")id:Int):Response<List<Anketa>>
    @GET("/student/{id}/anketataken")
    suspend fun getPoceteAnkete(@Path("id")id:String):Response<List<AnketaTaken>>
    @GET("/student/{id}/anketataken/{ktid}/odgovori")
    suspend fun getOdgovoriAnketa(@Path("id")id:String,@Path("ktid")ktid: Int):Response<List<Odgovor>>

    @POST("/grupa/{gid}/student/{id}")
    suspend fun upisiUGrupu(@Path("gid")gid:Int,@Path("id")id:String):Response<GetUpisiUGrupuRespone>
    @POST("/student/{id}/anketa/{kid}")
    suspend fun zapocniAnketu(@Path("id")id:String,@Path("kid")kid:Int):Response<AnketaTaken>
    @POST("/student/{id}/anketataken/{ktid}/odgovor")
    suspend fun postaviOdgovorAnketa(@Path("id")id:String,
                                     @Path("ktid")ktid:Int,
                                     @Body requestBody: DataModelOdgovor):Response<Odgovor>
}