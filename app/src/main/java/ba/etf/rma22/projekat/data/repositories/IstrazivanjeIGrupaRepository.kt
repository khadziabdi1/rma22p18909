package ba.etf.rma22.projekat.data.repositories

import ba.etf.rma22.projekat.AppDatabase
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.models.Grupa
import ba.etf.rma22.projekat.data.models.Istrazivanje
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object IstrazivanjeIGrupaRepository {
    private val db = AppDatabase.getInstance(MainActivity.context_!!)
    suspend fun getUpisaneGrupe():List<Grupa>{
        if(!MainActivity.getConnection())
            return db.grupaDao().getAll()
        val acHash:String=AccountRepository.getHash()
        return withContext(Dispatchers.IO){
            val response = ApiConfig.retrofit.getGrupeOdStudenta(acHash)
            val responseBody = response.body()
            if(responseBody == null) return@withContext listOf()
            else {
                for(i in responseBody){
                    db.grupaDao().insertGrupa(i)
                }
                return@withContext responseBody
            }
        }
    }
    suspend fun getIstrazivanja(offset:Int=0):List<Istrazivanje>{
        if(!MainActivity.getConnection())
            return db.istrazivanjeDao().getAll()
        return withContext(Dispatchers.IO){
            if(offset == 0){
                var abortFlag = false
                var count = 1
                val returnBody = arrayListOf<Istrazivanje>()

                while(!abortFlag){
                    val response = ApiConfig.retrofit.getIstrazivanja(count)
                    val responseBody = response.body() ?: return@withContext returnBody
                    if(responseBody.size < 5)abortFlag = true
                    returnBody.addAll(responseBody)
                    count ++
                }
                for(i in returnBody){
                    db.istrazivanjeDao().insertIstrazivanje(i)
                }
                return@withContext returnBody
            }
            val response = ApiConfig.retrofit.getIstrazivanja(offset)
            val responseBody = response.body()
            if(responseBody == null) return@withContext arrayListOf()
            else {
                for(i in responseBody){
                    db.istrazivanjeDao().insertIstrazivanje(i)
                }
                return@withContext responseBody
            }
        }
    }
    suspend fun getGrupe():List<Grupa>{
        if(!MainActivity.getConnection())
            return db.grupaDao().getAll()
        return withContext(Dispatchers.IO){
            val response = ApiConfig.retrofit.getGrupe()
            for(i in response.body()!!){
                db.grupaDao().insertGrupa(i)
            }
            return@withContext  response.body()!!
        }
    }
    suspend fun getGrupeZaIstrazivanje(idIstrazivanja:Int):List<Grupa>{
        return getGrupe().filter { it.istrazivanjeId == idIstrazivanja }
    }
    suspend fun upisiUGrupu(idGrupa:Int):Boolean{
        return withContext(Dispatchers.IO){
            val response = ApiConfig.retrofit.upisiUGrupu(idGrupa,AccountRepository.getHash())
            val responseBody = response.body()
            return@withContext responseBody!!.message.contains("Student")
        }
    }
}