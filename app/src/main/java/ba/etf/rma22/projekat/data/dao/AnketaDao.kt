package ba.etf.rma22.projekat.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma22.projekat.data.models.Anketa

@Dao
interface AnketaDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAnketa(anketa: Anketa)

    @Query("SELECT * FROM Anketa")
    suspend fun getAll(): List<Anketa>

    @Query("SELECT * FROM Anketa WHERE id= :id LIMIT 1")
    suspend fun getById(id: Int): Anketa


}