package ba.etf.rma22.projekat.data.repositories

import ba.etf.rma22.projekat.AppDatabase
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.models.DataModelOdgovor
import ba.etf.rma22.projekat.data.models.Odgovor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object OdgovorRepository {
    private val db = AppDatabase.getInstance(MainActivity.context_!!)

    suspend fun getOdgovoriAnketa(idAnkete:Int):List<Odgovor>{
        var lista = TakeAnketaRepository.getPoceteAnkete()
        var naci: Int = 0
        for(i in lista!!){
            if(i.AnketumId == idAnkete) naci = i.id
        }
        return withContext(Dispatchers.IO){
            val response = ApiConfig.retrofit.getOdgovoriAnketa(AccountRepository.getHash(), naci)
            val responseBody = response.body()
            if(responseBody == null) return@withContext listOf()
            else {
                for(i in responseBody)
                    db.odgovorDao().insertOdgovor(i)
                return@withContext responseBody
            }
        }
    }
    suspend fun postaviOdgovorAnketa (idAnketaTaken: Int, idPitanje: Long, odgovor: Int) : Int {
        return withContext(Dispatchers.IO) {
            val listaTaken = TakeAnketaRepository.getPoceteAnkete()?.find {it.id == idAnketaTaken}
                ?: return@withContext -1
            val dataModelOdgovor = DataModelOdgovor(odgovor, idPitanje, podesi((listaTaken.progres*100).toInt()))
            val response = ApiConfig.retrofit.postaviOdgovorAnketa(AccountRepository.getHash(), idAnketaTaken, dataModelOdgovor)
            val responseBody = response.body()
            val pr = getOdgovoriAnketa(listaTaken.AnketumId).size
            if (responseBody == null) return@withContext -1
            else return@withContext podesi((((pr + 0.0)/ PitanjeAnketaRepository.getPitanja(listaTaken.AnketumId)!!.size)*100).toInt())
        }
    }
    private fun podesi(p:Int):Int{
        val pi: Int = when {
            p<=10 -> 0
            p<30 -> 20
            p<50 -> 40
            p<70 -> 60
            p<90 -> 80
            else -> 100
        }
        return pi
    }
}