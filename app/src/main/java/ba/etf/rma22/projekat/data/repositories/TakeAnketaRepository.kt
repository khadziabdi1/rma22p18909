package ba.etf.rma22.projekat.data.repositories

import ba.etf.rma22.projekat.AppDatabase
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.models.AnketaTaken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object TakeAnketaRepository {
    private val db = AppDatabase.getInstance(MainActivity.context_!!)
    suspend fun zapocniAnketu(idAnkete:Int): AnketaTaken? {
        return withContext(Dispatchers.IO){
            val response = ApiConfig.retrofit.zapocniAnketu(AccountRepository.getHash(),idAnkete)
            if(response.body() == null) return@withContext null
            val responseBody = response.body() ?: return@withContext null
            db.anketaTakenDao().insertAnketaTaken(responseBody)
            return@withContext responseBody
        }
    }
    suspend fun getPoceteAnkete():List<AnketaTaken>?{
        return withContext(Dispatchers.IO){
            val response = ApiConfig.retrofit.getPoceteAnkete(AccountRepository.getHash())
            val responseBody = response.body()
            if(responseBody == null || responseBody.isEmpty()) return@withContext null
            else {
                for(i in responseBody) {
                    if(i != null)
                        db.anketaTakenDao().insertAnketaTaken(i)
                }
                return@withContext responseBody
            }
        }
    }
}