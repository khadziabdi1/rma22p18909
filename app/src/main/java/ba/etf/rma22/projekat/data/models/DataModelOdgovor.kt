package ba.etf.rma22.projekat.data.models

import com.google.gson.annotations.SerializedName
import okhttp3.RequestBody

data class DataModelOdgovor(
    @SerializedName("odgovor")var odgovor:Int,
    @SerializedName("pitanje")var idPitanje:Long,
    @SerializedName("progres")var progres:Int
)
