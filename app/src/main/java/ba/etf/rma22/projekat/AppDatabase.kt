package ba.etf.rma22.projekat

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import ba.etf.rma22.projekat.data.Converters
import ba.etf.rma22.projekat.data.dao.*
import ba.etf.rma22.projekat.data.models.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

@Database(entities = [Odgovor::class, Pitanje::class, Anketa::class,PitanjeAnketa::class, AnketaTaken::class, Grupa::class, Istrazivanje::class, Account::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDatabase: RoomDatabase() {
    abstract fun anketaDao(): AnketaDao
    abstract fun grupaDao(): GrupaDao
    abstract fun istrazivanjeDao(): IstrazivanjeDao
    abstract fun anketaTakenDao(): AnketaTakenDao
    abstract fun odgovorDao(): OdgovorDao
    abstract fun pitanjeAnketaDao(): PitanjeAnketaDao
    abstract fun pitanjeDao(): PitanjeDao
    abstract fun accountDao(): AccountDao
    companion object {
        private var INSTANCE: AppDatabase? = null
        fun getInstance(context: Context): AppDatabase {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = buildRoomDB(context)
                }
            }
            return INSTANCE!!
        }
        private fun buildRoomDB(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java,
                "RMA22DB"
            ).build()

    }
    fun clearTables(){
        GlobalScope.launch(Dispatchers.IO){
            this@AppDatabase.clearAllTables()
        }
    }
}