package ba.etf.rma22.projekat.viewmodel

import ba.etf.rma22.projekat.data.models.Grupa
import ba.etf.rma22.projekat.data.repositories.IstrazivanjeIGrupaRepository
import ba.etf.rma22.projekat.data.repositories.OdgovorRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class OdgovorViewModel {
    private val scope = CoroutineScope(Job() + Dispatchers.Main)
    fun postaviOdgovorAnketa (idAnketaTaken:Int,idPitanje:Long ,odgovor:Int,onSuccess: (progres: Int) -> Unit){
        scope.launch {
            val result = OdgovorRepository.postaviOdgovorAnketa(idAnketaTaken,idPitanje,odgovor)
            when (result) {
                else -> onSuccess.invoke(result)
            }
        }
    }
}