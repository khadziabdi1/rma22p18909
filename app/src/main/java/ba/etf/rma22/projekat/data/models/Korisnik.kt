package ba.etf.rma22.projekat.data.models

import java.util.*

data class Korisnik(
    val grupeNaIstrazivanjima: SortedMap<String,String>,
    val odgovori: SortedMap<String,String>
)
