package ba.etf.rma22.projekat.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.repositories.AnketaRepository
import ba.etf.rma22.projekat.viewmodel.AnketaListViewModel

class FragmentAnkete: Fragment() {
    private lateinit var listaAnketa: RecyclerView
    private lateinit var listaAnketaAdapter: AnketaListAdapter
    private var anketaListViewModel = AnketaListViewModel()
    private lateinit var birac: Spinner
    private lateinit var komunikator: Komunikator
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view:View = inflater.inflate(R.layout.ankete_fragment, container, false)
        komunikator = activity as Komunikator
        birac = view.findViewById(R.id.filterAnketa)
        listaAnketa = view.findViewById(R.id.listaAnketa)
        listaAnketa.layoutManager = GridLayoutManager(activity, 2)
        listaAnketaAdapter = AnketaListAdapter(listOf()){anketa -> prikaziAnketu(anketa)}
        listaAnketa.adapter = listaAnketaAdapter
        anketaListViewModel.getMyAnkete(onSuccess = ::onSuccess)

        val adapter = ArrayAdapter(requireActivity(),android.R.layout.simple_spinner_item,resources.getStringArray(R.array.FilterAnketa))
        birac.adapter = adapter
        birac.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when (parent?.getItemAtPosition(position).toString()) {
                    "Sve ankete" -> anketaListViewModel.getAll(onSuccess = ::onSuccess)
                    "Urađene ankete" -> anketaListViewModel.getDone(onSuccess = ::onSuccess)
                    "Buduće ankete" -> anketaListViewModel.getFuture(onSuccess = ::onSuccess)
                    "Prošle ankete" -> anketaListViewModel.getNotTaken(onSuccess = ::onSuccess)
                    else -> anketaListViewModel.getMyAnkete(onSuccess = ::onSuccess)
                }
            }
            override fun onNothingSelected(p0: AdapterView<*>?) {
                return
            }
        }
//        if(arguments?.getDouble("Progres") != null)
//        AnketaRepository.update(requireArguments().getString("Anketa")!!, requireArguments().getString("Istrazivanje")!!,requireArguments().getDouble("Progres"))
        return view
    }
    private fun onSuccess(ankete: List<Anketa>){
        listaAnketaAdapter.updateAnkete(ankete)
    }
    private fun prikaziAnketu(anketa: Anketa){
//        if(KorisnikRepository.user.grupeNaIstrazivanjima.containsKey(anketa.nazivIstrazivanja) && KorisnikRepository.user.grupeNaIstrazivanjima[anketa.nazivIstrazivanja]==anketa.nazivGrupe ) {
//            if(!anketaListViewModel.getFuture().contains(anketa))
//                komunikator.klikNaAnketu(anketa)
//        }
    }
    companion object{
        fun newInstance(): FragmentAnkete = FragmentAnkete()
    }

    override fun onResume() {
        super.onResume()
        when (birac.selectedItemPosition) {
            1 -> anketaListViewModel.getAll(onSuccess = ::onSuccess)
            2 -> anketaListViewModel.getDone(onSuccess = ::onSuccess)
            3 -> anketaListViewModel.getFuture(onSuccess = ::onSuccess)
            4 -> anketaListViewModel.getNotTaken(onSuccess = ::onSuccess)
            else -> anketaListViewModel.getMyAnkete(onSuccess = ::onSuccess)
        }
        komunikator.osvjeziPocetniEkran()
        return
    }

}