package ba.etf.rma22.projekat.viewmodel

import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.repositories.AnketaRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.util.*

class AnketaListViewModel {
    private val scope = CoroutineScope(Job()+Dispatchers.Main)

    fun getMyAnkete(onSuccess: (ankete: List<Anketa>) -> Unit){
        scope.launch {
            when(val result = AnketaRepository.getUpisane()){
                else -> onSuccess.invoke(result)
            }
        }
    }
    fun getAll(onSuccess: (ankete: List<Anketa>) -> Unit){
        scope.launch {
            when(val result = AnketaRepository.getAll()){
                else -> onSuccess.invoke(result)
            }
        }
    }

    fun getDone(onSuccess: (ankete: List<Anketa>) -> Unit){
        scope.launch {
            val result = AnketaRepository.getUpisane()
            result.filter {
                if(it.datumKraj == null) false
                else{
                    it.datumKraj!!.before(Calendar.getInstance().time)
                }
            }
            when(result){
                else -> onSuccess.invoke(result)
            }
        }
    }

    fun getFuture(onSuccess: (ankete: List<Anketa>) -> Unit){
        scope.launch {
            val result = AnketaRepository.getUpisane()
            result.filter { it.datumPocetak.after(Calendar.getInstance().time) }
            when(result){
                else -> onSuccess.invoke(result)
            }
        }
    }
    fun getNotTaken(onSuccess: (ankete: List<Anketa>) -> Unit){
        scope.launch {
            val result = AnketaRepository.getUpisane()
            result.filter {
                if(it.datumKraj == null)
                    false
                else
                    it.datumKraj!!.before(Calendar.getInstance().time)
            }
            result.forEach { it.prosla = true }
            when(result){
                else -> onSuccess.invoke(result)
            }
        }
    }
}