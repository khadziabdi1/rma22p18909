package ba.etf.rma22.projekat.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Grupa(
    @PrimaryKey @SerializedName("id")val id:Int,
    @SerializedName("naziv")val naziv:String,
    @SerializedName("IstrazivanjeId")val istrazivanjeId: Int
){
    override fun toString(): String {
        return naziv
    }

    override fun equals(other: Any?): Boolean {
        return (other as Grupa).id == this.id
    }
}
