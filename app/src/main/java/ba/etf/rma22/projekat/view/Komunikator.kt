package ba.etf.rma22.projekat.view

import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.Grupa

interface Komunikator {
    fun proslijediPodatke(g: Grupa)
    fun osvjeziPocetniEkran()
    fun klikNaAnketu(anketa: Anketa)
    fun predajAnketu(progres: Double,anketa: String,istrazivanje: String)
    fun zaustaviAnketu(anketa: String,istrazivanje: String)
}