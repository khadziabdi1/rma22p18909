package ba.etf.rma22.projekat.viewmodel

import ba.etf.rma22.projekat.data.models.DataModelOdgovor
import ba.etf.rma22.projekat.data.models.Grupa
import ba.etf.rma22.projekat.data.models.Odgovor
import ba.etf.rma22.projekat.data.repositories.IstrazivanjeIGrupaRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

object AccountViewModel {
    var odgovori:ArrayList<DataModelOdgovor> = arrayListOf()
    private val scope = CoroutineScope(Job() + Dispatchers.Main)
    fun dodajGrupu(id: Grupa, onSuccess: () -> Unit, onError: () -> Unit){
        scope.launch {
            when (IstrazivanjeIGrupaRepository.upisiUGrupu(id.id)){
                else -> onSuccess.invoke()
            }
        }
    }
    fun dodajOdgovor(idPitanja:Long, idOdgovora:Int,progres:Int){
        odgovori.add(DataModelOdgovor(idOdgovora,idPitanja,progres))
    }
}