package ba.etf.rma22.projekat.viewmodel

import ba.etf.rma22.projekat.data.models.Pitanje
import ba.etf.rma22.projekat.data.repositories.PitanjeAnketaRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

object PitanjeAnketaViewModel {
    private val scope = CoroutineScope(Job() + Dispatchers.Main)

    fun getPitanja(id:Int,onSuccess: (pitanja: List<Pitanje>) -> Unit){
        scope.launch {
            when(val result = PitanjeAnketaRepository.getPitanja(id)){
                else -> {
                    if(result == null || result.isEmpty()) onSuccess.invoke(listOf())
                    onSuccess.invoke(result!!)
                }
            }
        }
    }
}