package ba.etf.rma22.projekat.view

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.data.models.Anketa
import java.util.*

class AnketaListAdapter(private var ankete: List<Anketa>,private val onItemClicked: (Anketa)->Unit): RecyclerView.Adapter<AnketaListAdapter.AnketaViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AnketaListAdapter.AnketaViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.anketa_layout,parent,false)
        return AnketaViewHolder(view)
    }
    override fun getItemCount(): Int = ankete.size
    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: AnketaViewHolder, position: Int) {
        holder.anketaTitle.text = ankete[position].naziv
        holder.progressBar.progress =
            if((ankete[position].progres *100).toInt()%20>=10)
                (ankete[position].progres *100).toInt()+20-(ankete[position].progres *100).toInt()%20
            else (ankete[position].progres *100).toInt()-(ankete[position].progres *100).toInt()%20
        holder.nazivIstrazivanja.text = ankete[position].nazivIstrazivanja
        val cal: Calendar = Calendar.getInstance()
        if(ankete[position].datumKraj== null) {
            when {
                ankete[position].zavrsena -> {  //PLAVA
                    cal.time = ankete[position].datumRada!!
                    holder.datumAnkete.text =
                        "Anketa urađena: " + cal.get(Calendar.DAY_OF_MONTH) + "." + (cal.get(Calendar.MONTH)+1) + "." + cal.get(Calendar.YEAR)
                    holder.statusAnkete.setImageResource(R.drawable.plava)
                }
                nijePocela(ankete[position].datumPocetak) -> {  //ZUTA
                    cal.time = ankete[position].datumPocetak
                    holder.datumAnkete.text = "Vrijeme aktiviranja: " + cal.get(Calendar.DAY_OF_MONTH) + "." + (cal.get(Calendar.MONTH)+1) + "." + cal.get(Calendar.YEAR)
                    holder.statusAnkete.setImageResource(R.drawable.zuta)
                }
                else -> { //ZELENA
                    if(ankete[position].datumKraj == null){
                        holder.datumAnkete.text = "Nema kraja patnji!"
                    }else {
                        cal.time = ankete[position].datumKraj!!
                        holder.datumAnkete.text =
                            "Vrijeme zatvaranja: " + cal.get(Calendar.DAY_OF_MONTH) + "." + (cal.get(
                                Calendar.MONTH
                            ) + 1) + "." + cal.get(Calendar.YEAR)
                    }
                    holder.statusAnkete.setImageResource(R.drawable.zelena)
                }
            }
        }else{
        when {
            ankete[position].zavrsena -> {  //PLAVA
                cal.time = ankete[position].datumRada!!
                holder.datumAnkete.text =
                    "Anketa urađena: " + cal.get(Calendar.DAY_OF_MONTH) + "." + (cal.get(Calendar.MONTH)+1) + "." + cal.get(Calendar.YEAR)
                holder.statusAnkete.setImageResource(R.drawable.plava)
            }
            istekloVrijeme(ankete[position].datumKraj!!) -> { //CRVENA
                cal.time = ankete[position].datumKraj!!
                ankete[position].prosla=true
                holder.datumAnkete.text = "Anketa zatvorena: " + cal.get(Calendar.DAY_OF_MONTH) + "." + (cal.get(Calendar.MONTH)+1) + "." + cal.get(Calendar.YEAR)
                holder.statusAnkete.setImageResource(R.drawable.crvena)
            }
            nijePocela(ankete[position].datumPocetak) -> {  //ZUTA
                cal.time = ankete[position].datumPocetak
                holder.datumAnkete.text = "Vrijeme aktiviranja: " + cal.get(Calendar.DAY_OF_MONTH) + "." + (cal.get(Calendar.MONTH)+1) + "." + cal.get(Calendar.YEAR)
                holder.statusAnkete.setImageResource(R.drawable.zuta)
            }
            else -> { //ZELENA
                if(ankete[position].datumKraj == null){
                    holder.datumAnkete.text = "Nema kraja patnji!"
                }else {
                    cal.time = ankete[position].datumKraj!!
                    holder.datumAnkete.text =
                        "Vrijeme zatvaranja: " + cal.get(Calendar.DAY_OF_MONTH) + "." + (cal.get(
                            Calendar.MONTH
                        ) + 1) + "." + cal.get(Calendar.YEAR)
                }
                holder.statusAnkete.setImageResource(R.drawable.zelena)
            }
        }
        }
        holder.itemView.setOnClickListener{ onItemClicked(ankete[position])}
    }
    private fun istekloVrijeme(datum: Date): Boolean {
        return datum.before(Calendar.getInstance().time)
    }
    private fun nijePocela(datum: Date): Boolean{
        return datum.after(Calendar.getInstance().time)
    }
    @SuppressLint("NotifyDataSetChanged")
    fun updateAnkete(ankete: List<Anketa>){
        this.ankete = ankete
        notifyDataSetChanged()
    }
    inner class AnketaViewHolder(itemVeiw: View): RecyclerView.ViewHolder(itemVeiw){
        val anketaTitle: TextView = itemView.findViewById(R.id.anketaTitle)
        val progressBar: ProgressBar = itemView.findViewById(R.id.progresZavrsetka)
        val nazivIstrazivanja: TextView = itemVeiw.findViewById(R.id.nazivIstrazivanja)
        val datumAnkete: TextView = itemVeiw.findViewById(R.id.datumAnkete)
        val statusAnkete: ImageView = itemVeiw.findViewById(R.id.krug)
    }
}