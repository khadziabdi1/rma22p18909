package ba.etf.rma22.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import ba.etf.rma22.projekat.data.models.AnketaTaken

@Dao
interface AnketaTakenDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAnketaTaken(anketaTaken: AnketaTaken)
}