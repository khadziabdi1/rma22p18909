package ba.etf.rma22.projekat.data.repositories

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.URL

object ApiConfig {
    var baseURL ="https://rma22ws.herokuapp.com"
    fun postaviBaseURL(baseUrl:String) {
        this.baseURL = baseUrl
    }
    public val retrofit : Api = Retrofit.Builder()
        .baseUrl(URL(baseURL))
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(Api::class.java)
}