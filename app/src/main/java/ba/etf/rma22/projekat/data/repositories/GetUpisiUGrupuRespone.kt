package ba.etf.rma22.projekat.data.repositories

import com.google.gson.annotations.SerializedName

data class GetUpisiUGrupuRespone(
    @SerializedName("message")var message:String
)
