package ba.etf.rma22.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import ba.etf.rma22.projekat.data.models.PitanjeAnketa

@Dao
interface PitanjeAnketaDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPitanjeAnketa(pitanjeAnketa: PitanjeAnketa)
}