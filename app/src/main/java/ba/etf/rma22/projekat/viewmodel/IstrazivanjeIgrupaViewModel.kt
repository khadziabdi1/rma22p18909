package ba.etf.rma22.projekat.viewmodel

import ba.etf.rma22.projekat.data.models.Grupa
import ba.etf.rma22.projekat.data.repositories.IstrazivanjeIGrupaRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

object IstrazivanjeIgrupaViewModel {
    private val scope = CoroutineScope(Job() + Dispatchers.Main)

    fun getGrupe(onSuccess: (grupe: List<Grupa>) -> Unit){
        scope.launch {
            val result = IstrazivanjeIGrupaRepository.getGrupe()
            when (result) {
                else -> onSuccess.invoke(result)
            }
        }
    }
    fun getUpisaneGrupe(onSuccess: (grupe: List<Grupa>) -> Unit){
        scope.launch {
            val result = IstrazivanjeIGrupaRepository.getUpisaneGrupe()
            when (result) {
                else -> onSuccess.invoke(result)
            }
        }
    }

}