package ba.etf.rma22.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma22.projekat.data.models.Grupa

@Dao
interface GrupaDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertGrupa(grupa: Grupa)
    @Query("select * from grupa")
    suspend fun getAll(): List<Grupa>
}